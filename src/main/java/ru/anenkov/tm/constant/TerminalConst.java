package ru.anenkov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "Help";

    String ARG_HELP = "-h";

    String INFO_HELP = "Display terminal commands.";

    String CMD_VERSION = "Version";

    String ARG_VERSION = "-v";

    String INFO_VERSION = "Show version info.";

    String CMD_ABOUT = "About";

    String ARG_ABOUT = "-a";

    String INFO_ABOUT = "Show developer info.";

    String CMD_EXIT = "Exit";

    String INFO_EXIT = "Close application.";

    String CMD_INFO = "Info";

    String ARG_INFO = "-i";

    String INFO_INFO = "Display information about system";

}
